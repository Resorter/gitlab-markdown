## HELLO ALL!
### I have just followed the tutorials!
#### _And that's what I have._

[If you want to change your **mood**](https://www.youtube.com/watch?v=AiT7btxnyUQ&ab_channel=Hangar)

# So let's start with boring charts!

## Usual Information

| ------ | /////////// |
| ------ | ----------- |
| **Age**   | 26 |
| **From** | Alasehir, a small town of Manisa province in Turkey |
| **Studies**    | Bachelor of Architecture |

### Cross out the lines

~~You cannot express yourself with charts or stereotypical tools..~~

### Let's try something else!

- [ ] strawberry
- [ ] chicken
- [x] tuna fish
- [x] milk
- [ ] bread
- [ ] watermelon

### Nahhh!!! It wouldn't work too

That is so hard to tell about yourself with those tools!





