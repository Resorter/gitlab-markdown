# HELLO ! :raising_hand:

**THIS IS**   
**SHADY's**  
**BIO**  

*PS : It can be difficult to introduce yourself because you know yourself so well that you don't know where to begin.  
Let me try to see what kind of image you have of me based on my self-description.*



<img src="https://i.pinimg.com/originals/bd/5b/97/bd5b9753da76aa1586ed926e7bb82dc0.jpg" width=30% height=30%>  

|||||||||||||||||||||||||||||||||||||| **This Is Me** |||||||||||||||||||||||||||||||||||||||

*“Hey, I’m Shady Saleh . I grew up in Saudi Arabia but I am Egyptian , and lived there most of my life before moving to Egypt to start my Architecture journey at the German University in Cairo. I speak Arabic, English and lately some German. I love horse riding , and tend to socialize with people on daily basics , but this doesn't prevent me from wrapping myself in a blanket and binge-watch series without feeling guilty!”*  
### So...  

Lets start with my **EDUCATION** :  

**2015 /.** Finished my IGCSE's  :heavy_check_mark:  
**2020 /.** Finished my Bachelor Degree in Architecture and Urban Design  :heavy_check_mark:  
**2022 /.** Started my Masters Degree in Construction and Robotics  :wavy_dash:  

**WORK EXPERIENCE** :

* **3D CAD Designer at [360 Imaging](https://360imaging.com/)**  
*There i shifted a bit away from Architecture due to my huge urge to be involved in tech development . My job was to create FDA approved surgical dental guides for implant surgeries. Nothing related to Architecture but i had the chance to be creative.*  
* **Industrial Site Engineer at [Chema Foam](https://www.chema-foam.com/)**  
*For a year I had the chance to get hands-on experince in steel factories constrution sites. What I saw and experinced there was the main motivation to undergo this masters program .*
* **Interior Deginer at [Al-Futtaim IKEA](https://www.alfuttaim.com/)**
*For another year I wanted to experince small scale creations with an extra touch of elegance and improving living situations. So before coming to Aachen I decided to just give interior desiging a chance.*  

**DIGITAL SKILLS** :  

| Program      | Competence |
| ----------- | ----------- |
| AutoCad      | :black_small_square: :black_small_square: :black_small_square: :black_small_square:       |
| BIM Revit   | :black_small_square: :black_small_square: :black_small_square: :black_small_square:        |
| Rhino   | :black_small_square: :black_small_square: :black_small_square: :black_small_square:        |
| Grasshopper   | :black_small_square: :black_small_square:        |
| Photoshop   | :black_small_square: :black_small_square: :black_small_square: :black_small_square:        |
| Illustrator   | :black_small_square: :black_small_square:        |  

______

#### Now I will present to you my self **SWOT** analysis *( a study undertaken to identify internal strengths and weaknesses, as well as its external opportunities and threats.)* :


<img src="https://i.pinimg.com/originals/e6/66/42/e666428f2851994e1833610eb290fbdf.jpg">


#### **Last** thing about the career me  **My Portfolio** :
 
<img src="https://media1.giphy.com/media/sdabkheWjxfbUJBuOm/giphy.gif?cid=790b7611952881506e7dc8fc5d581fcfe4c75b8f93955135&rid=giphy.gif&ct=g" width=70% height=70%>

#### Yes it is fast .. but if you are intrestend to check it out do to my [ISSU](https://issuu.com/shmkamal/docs/shadykamalportfolio_compressed__2_) page .
___


## Moving on to the fun me.  

### As I said I love horse riding so let me show you some of what I experience back in Egypt :

<img src="https://media1.giphy.com/media/1JPJVTDJjAiVVAVdS5/giphy.gif?cid=790b7611f7d516577b78b5f6e9512a457011fc4f27e36af5&rid=giphy.gif&ct=g" width=70% height=70%>

### As for my constant urge to travel and get in touch with diffrent cultures here are some of them :

<img src="https://media3.giphy.com/media/BMu8sdADSxJGhH1Fdw/giphy.gif?cid=790b761129e896d25eecd2f294a7a2d3e3d37819a3591b0c&rid=giphy.gif&ct=g" width=70% height=70%>  

### TO SUM UP ME :

<img src="http://www.plantuml.com/plantuml/png/SoWkIImgoStCIybDBE3IKl1rvUBKKd0BzGEpWCnWVsTFHn_FA4y_Tp3Vmpyes5KXoDC59X0Iv1ZcwWFY0JdEZa6kha4WlGwfUIbGOm40">  



## I think that is all I can say about myself.  
# BYE. :v:




