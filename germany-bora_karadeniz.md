
# Writing in Markdown is not that hard!    


![markdown](https://upload.wikimedia.org/wikipedia/commons/4/48/Markdown-mark.svg)  


My name is Bora Karadeniz. 

Here i start my first steps with **Markdown**.  

#### Table of Contents:  

 * [About Markdown](#About Markdown)  
 * [About me](#About me)  
 * [Study and work](#Study and work)
    



## About Markdown  

With Markdown you can format texts. For example you can make words **bold**, _italic_ or even _**both**_.



For the respective creator Markdown is **easy** to read and **easy** to write. 

![gif](https://mention.com/wp-content/uploads/2017/11/golf-google-alerts-mention.gif)


**Here a small overwiev of some commands**

----  

 

<img src="http://code.ahren.org/wp-content/uploads/Screenshot-17.png" width="700" height="800">  



>_For more [**Visit MarkdownGuide!**](www.markdownguide.org)_  



----



## About me

I am from [**Dortmund**](https://www.google.de/maps/place/Dortmund/@51.5078011,7.3301801,11z/data=!3m1!4b1!4m5!3m4!1s0x47b91760bff07a11:0x427f28131548750!8m2!3d51.5135872!4d7.4652981). I was born and raised there.  

You might know the football club :soccer: BVB :soccer:  

  

<img src="https://www.bvb.de/var/ezdemo_site/storage/images/media/bilder/galeriebilder/g_monaco_fans_choreo/1942096-1-ger-DE/G_Monaco_Fans_Choreo_bvbnachrichtenbild_regular.jpg" width="600" height="350">  

I also played in the youth.



![gif2](https://debeste.de/upload/306eb55ad5660410b3d64fa9699011ab2520.gif)  
  


Things are going similarly for Borussia Dortmund this year. :chart_with_downwards_trend: :arrow_lower_right:  :arrow_upper_right: :arrow_lower_right: :arrow_upper_right:

However, things are going better for my parents' hometown [**Trabzon**](https://www.google.de/maps/place/Trabzon,+Ortahisar%2FTrabzon,+T%C3%BCrkei/@40.9929203,39.66122,12z/data=!3m1!4b1!4m5!3m4!1s0x40643c066bf2ec89:0xc4390aca748c2b71!8m2!3d41.0026969!4d39.7167633) in the north of Turkey :tr: , where my roots are.  

|          | club     | :soccer: | W | D | L | Goals | +/- | Pts |  
| ---      | ---      | ---      | --- | --- | --- | --- | --- | --- |
|    1     | Trabzonspor :trophy: | 33 | 21 | 10 | 2 | 60:28 | 32 | 73 |
|    2     | Fenerbahce  | 33 | 18 | 8 | 7 | 58:35 | 23 | 62 |
|    3     |  Konyaspor  | 33 | 18 | 7 | 8 | 56:36 | 20 | 61 |
|    :     |      :      | : |  : | : | : |   :   |  : |  : |  

Currently i live in Dortmund again and commute to Aachen. So I have to rely completely on Deutsche Bahn.  



 <img src="https://cdn.prod.www.spiegel.de/images/0834dbf0-0001-0004-0000-000000707233_w1600_r1.4018691588785046_fpx64.2_fpy50.jpg" width="600" height="350">    

 > **Deutsche Bahn calls it a timetable. I call it "non-binding departure recommendation with track suggestion".**  

 > **Deutsche Bahn is when you leave the house at 12 o'clock and still get the 11:43 train.**  

Just some jokes but [**here's**][1] how to get **_"Deutsche Bahn compensation in 5 minutes"_**.

 


[1]: https://www.pcwelt.de/news/Bahn-Entschaedigung-kuenftig-in-nur-5-Minuten-digital-beantragen-11033572.html



## Study and work  





















