# About me
## Who am I? ![who,who](https://melschwartz.com/wp-content/uploads/2021/05/H3-1.png)
> * Name: Rebeca García
> * Age: 28 years old
> * Nationality: Mexican 

I have a bachelor degree in **Architecture** and I worked one year in a _civil engineering office_ and 2 more years in a _design studio_ in Mexico City. Although in both jobs I had great professional experiences, I think that construction is very backward concerning technological progress; specifically in Mexico, the innovation processes are very slow or even null.  
#### My hobbies
![Some hobbies](http://www.plantuml.com/plantuml/png/FL7BYkCm4BpxA-fkmlv2s2naTqc6OMOWnz2HsdPZFOpKJiP_FnOMljMZkxfi7RDIKY_1UfgQ5jS5GtmyX3EQjaMSrSQOLjZUyLT62Vs6J-IddRmxMl2RoGnuHkTuUP1pTOT2NrBm4XtG9U5WSx5MxqeZOn8Jn5G9vuWk9jWaOSo_mAH0jVxFMM46SGxOihyIFTbLSgPaEUoLrY2DC34OJfhH70xu5ygd57YuuuF9hiCRESKuxkGFhJi5hlX2AW5P5yUvIgSObFqK4wK5jioYHjv8pO0trix9TzKkafL3gUBdBAQAJAaqtO-4VgP-veogVCPENvJscxU1b9-ScisTE5YeUDv-gb_GaHUt06YEgrTVymS0)

#### What are my interests?
Good question 

``` mermaid

stateDiagram-v2

    Interests
    Interests --> Professional
    Interests --> Personal
    Professional --> Innovation 
    Professional --> technology
    Parametric_Architecture --> Sustainability
     Parametric_Architecture --> Resilience
     Resilience --> Sustainability
    New_Construction_Processes --> 3D_Printing
    New_Construction_Processes --> Machine_Learning
    Sustainability --> New_Construction_Processes
    Sustainability --> New_Construction_Materials
    Professional --> Social_Commitment
    Innovation --> Parametric_Architecture
    technology --> Parametric_Architecture
    Personal --> Social_Commitment
    Personal --> Healthy_Lifestyle 
    Personal --> New_challenges
    Social_Commitment --> Affordable
    Social_Commitment --> Equitable   


```

Here some examples

![House in Mexico](https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/3d-home-1-1576099437.jpg)

![Another example](https://inteng-storage.s3.amazonaws.com/img/iea/zBwgQq8aGK/3d-printing-peri.jpeg)

**Interested?** 

Then watch [_this video_](https://www.youtube.com/watch?v=XHSYEH133HA) 
